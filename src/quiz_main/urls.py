from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from mongo_app.views import create_in_mongo, all_entries
from quiz.views import bitcoin, normalize_email

urlpatterns = [
    path("admin/", admin.site.urls),
    path("", include("quiz.urls")),
    path("api/", include("api.urls")),
    path("create_mongo/", create_in_mongo, name="create_mongo"),
    path("show_mongo/", all_entries, name="show_mongo"),
    path("bitcoin/", bitcoin),
    path("normalize/", normalize_email),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
