from django.conf import settings
from django.core.validators import MaxValueValidator
from django.db import models
from django.contrib.auth import get_user_model

from quiz.services.utils import generate_uuid


class BaseModel(models.Model):
    class Meta:
        abstract = True

    create_date = models.DateTimeField(null=True, auto_now_add=True)
    last_update = models.DateTimeField(null=True, auto_now=True)


class Quiz(BaseModel):
    class LEVEL_CHOICES(models.IntegerChoices):  # noqa:
        BASIC = 0, "Junior"
        MIDDLE = 1, "Middle"
        SENIOR = 2, "Senior"

    uuid = models.UUIDField(default=generate_uuid, db_index=True, unique=True)
    level = models.PositiveSmallIntegerField(
        choices=LEVEL_CHOICES.choices, default=LEVEL_CHOICES.MIDDLE
    )
    title = models.CharField(max_length=128)
    description = models.TextField(max_length=1024, null=True, blank=True)  # noqa
    image = models.ImageField(default="default.png", upload_to="covers")

    def __str__(self):
        return f"{self.title}"

    def questions_count(self):
        return self.questions.count()


class Question(BaseModel):
    quiz = models.ForeignKey(
        to=Quiz, related_name="questions", on_delete=models.CASCADE
    )
    order_number = models.PositiveSmallIntegerField(
        validators=[MaxValueValidator(settings.QUESTION_MAX_LIMIT)]
    )
    text = models.CharField(max_length=256)

    def __str__(self):
        return f"{self.text}"


class Choice(BaseModel):
    question = models.ForeignKey(
        to=Question, related_name="choices", on_delete=models.CASCADE
    )
    is_correct = models.BooleanField(default=False)
    text = models.CharField(max_length=128)

    def __str__(self):
        return f"{self.text}"


class Result(BaseModel):
    class STATE(models.IntegerChoices):  # noqa:
        NEW = 0, "New"
        IN_PROGRESS = 1, "In progress"
        FINISHED = 2, "Finished"

    quiz = models.ForeignKey(to=Quiz, related_name="results", on_delete=models.CASCADE)
    user = models.ForeignKey(
        to=get_user_model(), related_name="results", on_delete=models.CASCADE
    )
    state = models.PositiveSmallIntegerField(default=STATE.NEW, choices=STATE.choices)
    uuid = models.UUIDField(default=generate_uuid, db_index=True, unique=True)
    num_of_corrects_answers = models.PositiveSmallIntegerField(
        default=0, validators=[MaxValueValidator(settings.QUESTION_MAX_LIMIT)]
    )
