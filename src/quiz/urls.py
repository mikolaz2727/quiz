from django.urls import path

from quiz.views import Index, QuizDetails, QuestionView, ResultDetails

app_name = "quiz"


urlpatterns = [
    path("", Index.as_view(), name="index"),
    path("quiz/<uuid:uuid>", QuizDetails.as_view(), name="quiz_details"),
    path(
        "quiz/<uuid:uuid>/questions/<int:order>",
        QuestionView.as_view(),
        name="quiz_question",
    ),
    path("quiz/result/<uuid:uuid>", ResultDetails.as_view(), name="result_details"),
]
