import random
import time
from celery import shared_task
from accounts.models import CustomUser


@shared_task
def mine_bitcoin():
    time.sleep(random.randint(1, 10))


@shared_task
def normalize_email_task(filter):
    query_set = CustomUser.objects.filter(**filter)
    if query_set:
        for user in query_set:
            print(f"Working with {user.email}")
            user.save()
    else:
        print("Empty data")
