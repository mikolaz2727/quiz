from rest_framework.viewsets import ModelViewSet
from rest_framework.generics import RetrieveAPIView, ListAPIView

from api.serializers import UserSerializer, QuestionDetailSerializer, QuizSerializer
from accounts.models import CustomUser
from quiz.models import Question, Quiz


class UserViewSet(ModelViewSet):
    queryset = CustomUser.objects.all()
    serializer_class = UserSerializer


class QuestionDetailView(RetrieveAPIView):
    queryset = Question.objects.all()
    serializer_class = QuestionDetailSerializer

    def get_object(self):
        uuid = self.kwargs.get("uuid")
        order_number = self.kwargs.get("order_number")
        object = Question.objects.get(quiz__uuid=uuid, order_number=order_number)
        return object


class QuizListView(ListAPIView):
    # permission_classes = [IsAdminUser]
    queryset = Quiz.objects.all()
    serializer_class = QuizSerializer
